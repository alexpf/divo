package ru.alexpf.divo;

public abstract class ServiceNames {

    public static final String CALENDAR_SERVICE = "divoCalendarService";

    public static final String TAG_SERVICE = "divoTagService";

    public static final String USER_SERVICE = "divoUserService";
}

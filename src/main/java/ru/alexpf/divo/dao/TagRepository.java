package ru.alexpf.divo.dao;

import org.springframework.data.repository.CrudRepository;
import ru.alexpf.divo.dao.domain.Tag;

public interface TagRepository extends CrudRepository<Tag, String> {
}

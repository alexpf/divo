package ru.alexpf.divo.dao.domain.types;

/**
 * Created by Alexey on 30.11.2014.
 */
public enum EventType {
    WORK,
    CONTEST,
    REST
}

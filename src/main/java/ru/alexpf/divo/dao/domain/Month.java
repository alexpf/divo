package ru.alexpf.divo.dao.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

import static javax.persistence.CascadeType.ALL;

/**
 * Created by Alexey on 30.11.2014.
 */

@Entity
@Table(name = "MONTH")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Month {

    @Id
    @GeneratedValue
    private Integer id;

    @NonNull
    private int year;

    @NonNull
    private int number;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "month", cascade = ALL)
    private Collection<Day> days = new ArrayList<Day>();

    @NonNull
    @ManyToOne
    @JoinColumn(name = "tag_code")
    private Tag tag;
}

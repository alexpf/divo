package ru.alexpf.divo.dao.domain;

import lombok.*;
import ru.alexpf.divo.dao.domain.types.EventType;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by Alexey on 30.11.2014.
 */

@Entity
@Table(name = "DAY")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Day {

    @Id
    @GeneratedValue
    private Integer id;

    @Min(1)
    @Max(31)
    @NonNull
    private int number;

    @NotNull
    @NonNull
    private EventType eventType;

    private String comment;

    private LocalDateTime dateTime;

    @NotNull
    @NonNull // TODO: does lombok respect javax.validation's NotNull?
    @ManyToOne
    @JoinColumn(name = "month_id")
    private Month month;

}

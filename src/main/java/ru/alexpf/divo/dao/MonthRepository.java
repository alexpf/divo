package ru.alexpf.divo.dao;

import org.springframework.data.repository.CrudRepository;
import ru.alexpf.divo.dao.domain.Month;

import java.util.List;

public interface MonthRepository extends CrudRepository<Month, Integer> {

    List<Month> findAll();

    Month findByYearAndNumberAndTagCode(int year, int number, String tagCode);
}

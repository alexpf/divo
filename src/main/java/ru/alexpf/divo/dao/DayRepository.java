package ru.alexpf.divo.dao;

import org.springframework.data.repository.CrudRepository;
import ru.alexpf.divo.dao.domain.Day;

public interface DayRepository extends CrudRepository<Day, Integer> {
}

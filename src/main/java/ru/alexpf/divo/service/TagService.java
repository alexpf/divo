package ru.alexpf.divo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alexpf.divo.dao.TagRepository;
import ru.alexpf.divo.dao.domain.Tag;
import ru.alexpf.divo.dao.domain.types.PredefinedTagCode;

import javax.annotation.PostConstruct;
import java.util.HashMap;

import static ru.alexpf.divo.ServiceNames.TAG_SERVICE;
import static ru.alexpf.divo.dao.domain.types.PredefinedTagCode.COMMON;

@Service(TAG_SERVICE)
public class TagService {

    private static HashMap<PredefinedTagCode, String> tagTranslations = new HashMap<>();

    static {
        tagTranslations.put(COMMON, "Общие события");
        tagTranslations.put(PredefinedTagCode.YANA, "Яна");
    }

    @Autowired
    private TagRepository repository;

    @PostConstruct
    public void setup() {

        for (PredefinedTagCode code : PredefinedTagCode.values()) {
            if (!repository.exists(code.name()))
                repository.save(Tag.builder()
                        .code(code.name())
                        .russianTitle(tagTranslations.get(code))
                        .build());
        }
    }

    public Iterable<Tag> getAllTags() {
        return repository.findAll();
    }
}

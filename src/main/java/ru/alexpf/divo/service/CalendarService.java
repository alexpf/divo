package ru.alexpf.divo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alexpf.divo.dao.DayRepository;
import ru.alexpf.divo.dao.MonthRepository;
import ru.alexpf.divo.dao.TagRepository;
import ru.alexpf.divo.dao.domain.Day;
import ru.alexpf.divo.dao.domain.Month;
import ru.alexpf.divo.dao.domain.Tag;
import ru.alexpf.divo.web.api.DiagResult;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.Month.of;
import static ru.alexpf.divo.ServiceNames.CALENDAR_SERVICE;
import static ru.alexpf.divo.dao.domain.types.EventType.REST;
import static ru.alexpf.divo.web.api.DiagStatus.ERROR;
import static ru.alexpf.divo.web.api.DiagStatus.OK;

@Service(CALENDAR_SERVICE)
public class CalendarService {

    @Autowired
    private MonthRepository monthRepository;

    @Autowired
    private DayRepository dayRepository;

    @Autowired
    private TagRepository tagRepository;

    public Month getOrGenerateMonth(int year, int monthNumber, String tagCode) {

        Month month = monthRepository.findByYearAndNumberAndTagCode(year, monthNumber, tagCode);
        if (month != null) return month;

        Tag tag = tagRepository.findOne(tagCode);
        if (tag == null) throw new RuntimeException("Tag " + tagCode + " is not found");
        month = new Month(year, monthNumber, tag);

        int length = daysInThisMonth(year, monthNumber);
        for (int i = 1; i <= length; i++)
            month.getDays().add(new Day(i, REST, month));

        return monthRepository.save(month);
    }

    private int daysInThisMonth(int year, int monthNumber) {
        LocalDate date = LocalDate.of(year, of(monthNumber), 1);
        return date.getMonth().length(date.isLeapYear());
    }

    public Month getMonth(int year, int monthNumber, String tagCode) {

        return monthRepository.findByYearAndNumberAndTagCode(year, monthNumber, tagCode);
    }

    public void updateDay(int id, Day update) {
        Day day = dayRepository.findOne(id);
        if (update.getComment() != null) day.setComment(update.getComment());
        if (update.getEventType() != null) day.setEventType(update.getEventType());
        if (update.getDateTime() != null) day.setDateTime(update.getDateTime());
        dayRepository.save(day);
    }

    public DiagResult doDiagnostics() {

        StringBuilder result = new StringBuilder();

        List<Month> allMonths = monthRepository.findAll();
        checkAllMonthsAreUnique(result, allMonths);
        checkProperNumOfDaysInEachMonth(result, allMonths);

        String resultString = result.toString();
        if (resultString.isEmpty()) {
            return new DiagResult(OK, "Диагностика пройдена успешно.");
        } else {
            return new DiagResult(ERROR, resultString);
        }
    }

    private void checkProperNumOfDaysInEachMonth(StringBuilder result, List<Month> allMonths) {
        // There shall be proper number of days for each month
        for (Month month : allMonths) {
            int id = month.getId();
            int numOfDayRecords = 0;
            if (month.getDays() != null) numOfDayRecords = month.getDays().size();
            int expectedNumOfDays = daysInThisMonth(month.getYear(), month.getNumber());
            int monthNumber = month.getNumber();
            int yearNumber = month.getYear();
            String tagCode = month.getTag().getCode();

            if (numOfDayRecords != expectedNumOfDays) {
                String message = MessageFormat.format(
                        "Найдено {0} дней для месяца id={1} {2}-{3}-{4}, ожидалось {5}. ",
                        numOfDayRecords,
                        id,
                        String.valueOf(yearNumber),
                        monthNumber,
                        tagCode,
                        expectedNumOfDays
                );
                result.append(message);
            }
        }
    }

    private void checkAllMonthsAreUnique(StringBuilder result, List<Month> allMonths) {

        Collection<String> messages = new ArrayList<>();

        // There shall be 0..1 months for each year and tag
        for (Month month : allMonths) {
            int monthNumber = month.getNumber();
            int yearNumber = month.getYear();
            String tagCode = month.getTag().getCode();

            int numOfSuchMonths = allMonths
                    .stream()
                    .filter(m -> m.getNumber() == monthNumber &&
                            m.getYear() == yearNumber &&
                            m.getTag().getCode().equals(tagCode))
                    .collect(Collectors.toList())
                    .size();

            if (numOfSuchMonths != 1) {
                String message = MessageFormat.format(
                        "Найдено {0} записей для месяца {1}-{2}-{3}. ",
                        numOfSuchMonths,
                        String.valueOf(yearNumber),
                        monthNumber,
                        tagCode
                );

                if (!messages.contains(message)) messages.add(message);
            }
        }

        messages.forEach(result::append);
    }
}

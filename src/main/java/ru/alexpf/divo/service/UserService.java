package ru.alexpf.divo.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import ru.alexpf.divo.web.api.UserDetails;

import java.util.List;
import java.util.stream.Collectors;

import static ru.alexpf.divo.ServiceNames.USER_SERVICE;

@Service(USER_SERVICE)
public class UserService {

    public UserDetails getUserDetails() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roles = user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new UserDetails(user.getUsername(), roles);
    }
}

package ru.alexpf.divo;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.SpringBootWebSecurityConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.net.URI;
import java.net.URISyntaxException;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
@EnableAutoConfiguration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application {

    @Value("${spring.datasource.username}")
    private String defaultUsername;

    @Value("${spring.datasource.password}")
    private String defaultPassword;

    @Value("${spring.datasource.url}")
    private String defaultUrl;

    @Value("${spring.datasource.driverClassName}")
    private String defaultDriverClassName;

    @Bean
    @Order(HIGHEST_PRECEDENCE)
    CharacterEncodingFilter characterEncodingFilter() {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        return filter;
    }

    @Bean
    public DataSource dataSource() {

        URI dbUri;
        try {
            String username = defaultUsername;
            String password = defaultPassword;
            String url = defaultUrl;

            String dbProperty = System.getProperty("database.url");
            if (dbProperty != null) {
                dbUri = new URI(dbProperty);

                username = dbUri.getUserInfo().split(":")[0];
                password = dbUri.getUserInfo().split(":")[1];
                url = "jdbc:postgresql://" + dbUri.getHost() + ':' +
                        dbUri.getPort() + dbUri.getPath();
            }

            DataSource basicDataSource = new DataSource();
            basicDataSource.setUrl(url);
            basicDataSource.setUsername(username);
            basicDataSource.setPassword(password);
            basicDataSource.setDriverClassName(defaultDriverClassName);

            return basicDataSource;

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Configuration
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
    protected static class ApplicationWebSecurityConfigurerAdapter extends
            WebSecurityConfigurerAdapter {

        @Autowired
        private SecurityProperties security;

        // Initially copied from
        // org.springframework.boot.autoconfigure.security.SpringBootWebSecurityConfiguration
        @Override
        protected void configure(HttpSecurity http) throws Exception {

            if (this.security.isRequireSsl()) {
                http.requiresChannel().anyRequest().requiresSecure();
            }

            if (!this.security.isEnableCsrf()) {
                http.csrf().disable();
            }

            // No cookies for application endpoints by default
            http.sessionManagement().sessionCreationPolicy(this.security.getSessions());

            SpringBootWebSecurityConfiguration.configureHeaders(http.headers(),
                    this.security.getHeaders());

            http.exceptionHandling().authenticationEntryPoint(entryPoint());
            http.httpBasic();
            http.requestMatchers().antMatchers("/**");
            http.authorizeRequests()
                    .antMatchers("/api/day/**").hasRole("USER")
                    .antMatchers("/api/diagnostics").hasRole("ADMIN")
                    .anyRequest().authenticated();
        }

        @Override
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication()
                    .withUser("alexey")
                        .password(System.getProperty("alexey.password"))
                        .roles("ADMIN", "USER")
                    .and()
                    .withUser("yana")
                        .password(System.getProperty("yana.password"))
                        .roles("USER")
                    .and()
                    .withUser("guest").password("guest").roles("GUEST");
        }

        private AuthenticationEntryPoint entryPoint() {
            BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
            entryPoint.setRealmName("Divo");
            return entryPoint;
        }

    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

package ru.alexpf.divo.aop;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.alexpf.divo.aop.strategy.ExclusionStrategy;

import java.text.DecimalFormat;

import static org.apache.commons.lang3.ArrayUtils.indexOf;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.*;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.core.Ordered.LOWEST_PRECEDENCE;


@Aspect
@Component
public class ControllerLoggingAspect {
    public static final String COMMA = ", ";

    private static final Logger logger = getLogger("FIO2_REST_REQUEST_RESPONSE_LOGGER");

    @Pointcut("(within(ru.alexpf.divo.web.controller..*))")
    public void requiredCoverage() {
    }

    @Order(LOWEST_PRECEDENCE)
    @Around("requiredCoverage()")
    protected Object log(ProceedingJoinPoint pjp) throws Throwable {
        StringBuilder message = new StringBuilder();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Object result = null;
        try {
            result = pjp.proceed();
            return result;
        } catch (Exception ex) {

            result = ex;
            throw ex;
        } finally {
            stopWatch.stop();

            appendRequestLog(message, pjp);
            appendResponseLog(message, result);
            appendPerformanceLog(message, stopWatch);

            if (logger.isInfoEnabled()) logger.info(message.toString());
        }
    }


    private void appendPerformanceLog(StringBuilder message, StopWatch stopWatch) {
        message.append("\n")
                .append("\tduration: ")
                .append(new DecimalFormat("0.000").format(stopWatch.getTotalTimeMillis() / 1000.00))
                .append(" sec");
    }

    private void appendResponseLog(StringBuilder message, Object result) {
        String response = null;
        try {
            if (result instanceof Exception) response = ((Exception) result).getMessage();
            else response = new Gson().toJson(result);
        } catch (Exception ex) {
            if (logger.isDebugEnabled()) logger.debug("While trying to serialize composite object to json", ex);
        }
        message.append("\n").append("\tresponse: ").append(response);
    }

    private void appendRequestLog(StringBuilder message, ProceedingJoinPoint pjp) {
        String request = null;
        try {
            request = getRequestDescription(pjp);
        } catch (Exception ex) {
            if (logger.isDebugEnabled()) logger.debug("While trying to serialize composite object to json", ex);
        }

        message.append("\n").append("\trequest: ").append(request);
    }

    private String getParameterName(final String[] parameters, final int index) {
        if (isEmpty(parameters) || index < 0) return null;

        for (String parameter : parameters) {
            if (indexOf(parameters, parameter) == index) return parameter;
        }

        return null;
    }

    private String getRequestDescription(final JoinPoint jp) {
        String description;
        if (jp == null || jp.getSignature() == null || !(jp.getSignature() instanceof MethodSignature))
            return null;

        MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        if (isBlank(methodSignature.getName())) return null;

        description = methodSignature.getDeclaringType().getSimpleName() + "." + methodSignature.getName();
        String requestMethods = getRequestMethods(methodSignature);
        if (StringUtils.isNotBlank(requestMethods)) {
            description = description.concat(" [");
            description = description.concat(requestMethods);
            description = description.concat("]");
        }

        description = description.concat(": ");
        if (isEmpty(jp.getArgs())) {
            description = description.concat("no arguments");
            return description;
        }

        for (Object arg : jp.getArgs()) {
            if (arg == null) continue;
            else if (StringUtils.isEmpty(arg.getClass().getPackage().getName())) continue;

            if (!contains(arg.getClass().getPackage().getName(), "java.")
                    && !equalsIgnoreCase(arg.getClass().getPackage().getName(), "ru.alexpf.divo.web.api")) {
                continue;
            }

            if (equalsIgnoreCase(arg.getClass().getPackage().getName(), "ru.alexpf.divo.web.api")) {
                description = description.concat(processCompositeObject(arg));
                if (indexOf(jp.getArgs(), arg) != (jp.getArgs().length - 1)) description = description.concat(COMMA);
            } else if (contains(arg.getClass().getPackage().getName(), "java.")) {
                String parameterName = getParameterName(methodSignature.getParameterNames(), indexOf(jp.getArgs(), arg));
                if (isBlank(parameterName)) continue;

                description = description.concat(processObject(parameterName, arg));
                if (indexOf(jp.getArgs(), arg) != (jp.getArgs().length - 1)) description = description.concat(COMMA);
            }
        }

        return description;
    }

    private String getRequestMethods(final MethodSignature signature) {
        String requestMethods = null;
        if (signature == null) return null;

        RequestMapping requestMapping = signature.getMethod().getAnnotation(RequestMapping.class);
        if (requestMapping == null || isEmpty(requestMapping.method())) return null;

        for (RequestMethod requestMethod : requestMapping.method()) {
            if (requestMethods == null) {
                requestMethods = requestMethod.name();
            } else {
                requestMethods = requestMethods.concat(requestMethod.name());
                if (indexOf(requestMapping.method(), requestMethod) != (requestMapping.method().length - 1))
                    requestMethods = requestMethods.concat(COMMA);
            }
        }

        return requestMethods;
    }

    private String processCompositeObject(final Object source) {
        String description = "";
        if (source == null) return description;

        try {
            description = new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy()).create().toJson(source);
        } catch (Exception e) {
            if (logger.isDebugEnabled()) logger.debug("While trying to serialize composite object to json");
        }

        return description;
    }

    private String processObject(final String parameterName, final Object source) {
        String description = "";
        if (source == null || isBlank(parameterName)) return description;

        return parameterName.concat(": ").concat(String.valueOf(source));
    }
}

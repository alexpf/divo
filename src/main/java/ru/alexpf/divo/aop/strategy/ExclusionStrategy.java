package ru.alexpf.divo.aop.strategy;

import com.google.gson.FieldAttributes;
import ru.alexpf.divo.aop.annotation.Exclude;

public class ExclusionStrategy implements com.google.gson.ExclusionStrategy {

    public boolean shouldSkipClass(Class<?> clazz) {
        return clazz.getAnnotation(Exclude.class) != null;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(Exclude.class) != null;
    }
}

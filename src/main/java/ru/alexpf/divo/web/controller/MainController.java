package ru.alexpf.divo.web.controller;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import ru.alexpf.divo.dao.domain.types.EventType;
import ru.alexpf.divo.service.CalendarService;
import ru.alexpf.divo.service.TagService;
import ru.alexpf.divo.service.UserService;
import ru.alexpf.divo.web.api.*;

import javax.validation.Valid;
import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;
import static ru.alexpf.divo.ServiceNames.*;

@RestController
@RequestMapping("api")
public class MainController {

    private MapperFacade mapper = createMapper();

    @Autowired
    @Qualifier(CALENDAR_SERVICE)
    private CalendarService calendarService;

    @Autowired
    @Qualifier(TAG_SERVICE)
    private TagService tagService;

    @Autowired
    @Qualifier(USER_SERVICE)
    private UserService userService;

    @RequestMapping(value = "month", method = GET)
    public Month getMonth(@RequestParam(value = "year") int year,
                          @RequestParam(value = "number") int number,
                          @RequestParam(value = "tag") String tag) {
        return mapper.map(
                calendarService.getMonth(year, number, tag),
                Month.class);
    }

    @RequestMapping(value = "month", method = POST)
    public Month generateMonth(@RequestParam(value = "year") int year,
                               @RequestParam(value = "number") int number,
                               @RequestParam(value = "tag") String tag) {
        return mapper.map(
                calendarService.getOrGenerateMonth(year, number, tag),
                Month.class);
    }

    @RequestMapping(value = "day/{id}", method = PUT)
    public void updateDay(@PathVariable int id, @RequestBody @Valid Day update) {
        calendarService.updateDay(id, mapper.map(update, ru.alexpf.divo.dao.domain.Day.class));
    }

    @RequestMapping(value = "tag", method = GET)
    public Collection<Tag> getAllTags() {
        return mapper.mapAsList(tagService.getAllTags(), Tag.class);
    }

    @RequestMapping(value = "user", method = GET)
    public UserDetails getUserDetails() {
        return userService.getUserDetails();
    }

    @RequestMapping(value = "diagnostics", method = GET)
    public DiagResult getDiagnostics() {
        return calendarService.doDiagnostics();
    }

    private MapperFacade createMapper() {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();

        class DayConverter extends CustomConverter<Day, ru.alexpf.divo.dao.domain.Day> {

            @Override
            public ru.alexpf.divo.dao.domain.Day convert(Day day, Type<? extends ru.alexpf.divo.dao.domain.Day> type) {
                ru.alexpf.divo.dao.domain.Day domainDay = new ru.alexpf.divo.dao.domain.Day();
                if (day.getId() != null) domainDay.setId(day.getId());
                if (day.getNumber() != null) domainDay.setNumber(day.getNumber());
                if (day.getEventType() != null) domainDay.setEventType(EventType.valueOf(day.getEventType().name()));
                if (day.getComment() != null) domainDay.setComment(day.getComment());
                if (day.getDateTime() != null) domainDay.setDateTime(day.getDateTime());
                return domainDay;
            }
        }

        factory.getConverterFactory().registerConverter(new DayConverter());

        return factory.getMapperFacade();
    }

}

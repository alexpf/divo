package ru.alexpf.divo.web.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UserDetails {

    public String name;

    public List<String> roles;
}

package ru.alexpf.divo.web.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DiagResult {

    private DiagStatus status;

    private String details;
}

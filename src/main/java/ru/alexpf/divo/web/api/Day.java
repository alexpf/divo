package ru.alexpf.divo.web.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import ru.alexpf.divo.dao.domain.types.EventType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(NON_NULL)
@Getter
@Setter
public class Day {

    private Integer id;

    @Min(1)
    @Max(31)
    private Integer number;

    private EventType eventType;

    private String comment;

    private LocalDateTime dateTime;
}

package ru.alexpf.divo.web.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tag {

    public String code;

    public String russianTitle;
}

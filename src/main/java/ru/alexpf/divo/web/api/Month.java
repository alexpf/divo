package ru.alexpf.divo.web.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

/**
 * Created by Alexey on 29.11.2014.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Month {

    private int year;

    private int number;

    private Collection<Day> days;
}

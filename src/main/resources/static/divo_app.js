// TODO: move all prompts to constants, and reuse in the tests

var divoApp = angular.module("divoApp", ['ui.bootstrap']);

divoApp.directive("calendar", function() {
    return {
        restrict: "E",
        templateUrl: "calendar.html",
        scope: {
            month: "=",
            dayOffset: "=",
            showCurrent: "=",
            onClick: "&"
        },
        link: function(scope) {
            scope.$watch('showCurrent', function() {
                if (scope.showCurrent == false) delete scope.currentCalendarDay;
            });
            scope.$watch('month', function() {
                delete scope.currentCalendarDay;
            });
        },
        controller: ['$scope', function($scope) {
            $scope.calendarRowCellClass = function(index) {

                var cellClass;

                if (index % 7 == 1) cellClass = "col-xs-1 col-xs-offset-2 text-center";
                else cellClass = "col-xs-1 text-center";

                var dayIndex = index - $scope.dayOffset;

                if (!$scope.month) return cellClass;
                if (dayIndex <= 0 || dayIndex > $scope.month.days.length) return cellClass;
                if ($scope.currentCalendarDay != null &&
                    $scope.currentCalendarDay == $scope.month.days[dayIndex-1])
                    cellClass += " current-cell";

                return cellClass;
            };

            $scope.calendarButtonClass = function(index) {
                if (!$scope.month) return "btn steel";

                var dayIndex = index - $scope.dayOffset;

                if (dayIndex <= 0 || dayIndex > $scope.month.days.length) return "btn steel";

                switch ($scope.month.days[dayIndex-1].eventType) {
                    case "REST": return "btn steel text-center";
                    case "WORK": return "btn yellow text-center"
                    case "CONTEST": return "btn red text-center";
                }
                return "btn steel";
            };

            $scope.showCalendarDayCell = function(index) {
                var dayIndex = index - $scope.dayOffset;
                if (!$scope.month) return false;
                if (dayIndex <= 0 || dayIndex > $scope.month.days.length) return false;
                else return true;
            };

            $scope.getTooltip = function(index) {
                if ($scope.showCurrent) {
                    return "";
                }
                else {
                    var dayIndex = index - $scope.dayOffset;
                    if (!$scope.month) return "";
                    if (dayIndex <= 0 || dayIndex > $scope.month.days.length) return "";
                    return $scope.month.days[dayIndex-1].comment || "";
                }
            };

            $scope.dayLabel = function(index) {
                if (!$scope.month || !$scope.month.days) return "";

                var label = index - $scope.dayOffset;
                if (!label) return "";
                if (label <= 0) return "";
                if (label > $scope.month.days.length) return "";
                return String(label);
            };

            $scope.dayClick = function(index) {
                if (!$scope.showCurrent) return;
                if (!$scope.month) return;

                var dayIndex = index - $scope.dayOffset;
                if (dayIndex <= 0 || dayIndex > $scope.month.days.length) return;

                if ($scope.currentCalendarDay == null ||
                    $scope.currentCalendarDay != $scope.month.days[dayIndex-1]) {
                    $scope.currentCalendarDay = $scope.month.days[dayIndex-1];
                }

                $scope.onClick({id:dayIndex});
            };
        }]
    }
});


divoApp.controller("calendarCtrl", function ($scope, $http, $timeout, dateService) {

    var monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май",
        "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

    var date = dateService.getDate();
    var monthNumber = date.getMonth();
    var yearNumber = date.getFullYear();
    var daysInCurrentMonth;
    var currentTagCode = "YANA";

    $scope.editMode = false;
    $scope.alerts = [];
    $scope.progressMessage = null;
    $scope.dayOffset = null;

    $scope.data = {};
    $scope.tags = {};

    getInitialInformation();

    function getInitialInformation() {
        $scope.progressMessage = "Загружаю данные пользователя...";
        $http.get("/api/user")
            .success(function(data) {
                $scope.userDetails = data;
                getAllTags();
            })
            .error(function (error) {
                showError('Произошла ошибка. Не удалось загрузить информацию о пользователе.', error);
            })
            .finally(function() {
                $scope.progressMessage = null;
            });
    }

    function getAllTags() {

        $scope.progressMessage = "Загружаю теги...";

        $http.get("/api/tag")
            .success(function(data) {
                data.sort(function(tag1, tag2) {
                    if (tag1.russianTitle < tag2.russianTitle)
                        return -1;
                    if (tag1.russianTitle > tag2.russianTitle)
                        return 1;
                    return 0;
                });
                $scope.tags = data;
                updateScope();
            })
            .error(function (error) {
                showError('Произошла ошибка. Не удалось загрузить список тегов.', error);
            })
            .finally(function() {
                $scope.progressMessage = null;
            });
    };

    $scope.tagItemClass = function(code) {
        if (code == currentTagCode) return "tag-item-selected";
        else return "tag-item";
    };

    $scope.selectCurrentTag = function(code) {
        if (currentTagCode != code) {
            currentTagCode = code;
            updateCalendarInScope();
        }
    };

    $scope.showCalendar = function() {
        if ($scope.data.currentMonth) return true;
        else return false;
    };

    $scope.showEditButton = function() {
        if (!$scope.userDetails) return false;
        if ($scope.userDetails.roles.indexOf('ROLE_USER') > -1) return true;
        else return false;
    }

    $scope.isAdmin = function() {
        if (!$scope.userDetails) return false;
        if ($scope.userDetails.roles.indexOf('ROLE_ADMIN') > -1) return true;
        else return false;
    }

    function updateScope() {
        $scope.dayOffset = dateService.calculateDayOffset(yearNumber, monthNumber);
        daysInCurrentMonth = dateService.daysInMonth(yearNumber, monthNumber);

        updateCalendarInScope();
    };

    function updateCalendarInScope() {
        var dataUrl = "/api/month?year=" + yearNumber +
            "&number=" + (Number(monthNumber)+1) +
            "&tag=" + currentTagCode;
        $scope.progressMessage = "Загружаю месяц...";

        $http.post(dataUrl)
            .success(function (data) {
                data.days.sort(function(day1, day2) {return day1.number - day2.number;})
                $scope.data.currentMonth = data;
                delete $scope.currentCalendarDay;
            })
            .error(function (error) {
                $scope.data.error = error;
                showError('Произошла ошибка. Не удалось загрузить календарь месяца.', error);
            })
            .finally(function() {
                $scope.progressMessage = null;
            });

        $scope.month = monthNames[monthNumber] + " " + yearNumber;
        delete $scope.currentCalendarDay;
    }

    $scope.progressShow = function() {
        return ($scope.progressMessage != null);
    };

    $scope.dayClick = function(dayIndex) {
        if (!$scope.editMode) return;
        if (!$scope.data.currentMonth) return;
        if (dayIndex <= 0 || dayIndex > daysInCurrentMonth) return;

        if ($scope.currentCalendarDay == null ||
            $scope.currentCalendarDay != $scope.data.currentMonth.days[dayIndex-1]) {
            $scope.currentCalendarDay = $scope.data.currentMonth.days[dayIndex-1];
        }
        else {

            var eventType = $scope.currentCalendarDay.eventType;
            var newStatus = "REST";
            switch (eventType) {
                case "REST": newStatus = "WORK"; break;
                case "WORK": newStatus = "CONTEST"; break;
                case "CONTEST": newStatus = "REST"; break;
            }

            $scope.currentCalendarDay.eventType = newStatus;

            var dataUrl = "/api/day/" + $scope.currentCalendarDay.id;
            var request = {eventType: newStatus};
            $scope.progressMessage = "Обновляю статус дня...";

            $http.put(dataUrl, request)
                .error(function(error) {
                    showError('Произошла ошибка. Не удалось обновить день.', error);
                })
                .finally(function() {
                    $scope.progressMessage = null;
                });
        }
    };

    $scope.clickBack = function() {
        var result = dateService.previousMonthAndYear(monthNumber, yearNumber);
        monthNumber = result.month;
        yearNumber = result.year;
        updateScope();
    };

    $scope.clickForward = function() {
        var result = dateService.nextMonthAndYear(monthNumber, yearNumber);
        monthNumber = result.month;
        yearNumber = result.year;
        updateScope();
    };

    $scope.editButtonCaption = function() {
        if ($scope.editMode)
            return "Просмотр";
        else
            return "Редактирование";
    };

    $scope.toggleEditMode = function() {
        // TODO: do not switch to edit mode if no month data is present
        delete $scope.currentCalendarDay;

        if ($scope.editMode)
            $scope.editMode = false;
        else
            $scope.editMode = true;
    };

    $scope.onCommentChange = function() {
        var dayId = $scope.currentCalendarDay.id;
        var commentUpdate = $scope.currentCalendarDay.comment;

        cancelContentTimer();
        $scope.contentUpdateTimer = $timeout(function() {
            var dataUrl = "/api/day/" + dayId;
            var request = {comment: commentUpdate};

            $scope.progressMessage = "Обновляю описание события...";
            $http.put(dataUrl, request)
                .error(function(error) {
                    showError('Произошла ошибка. Не удалось обновить описание события.', error);
                })
                .finally(function() {
                    $scope.progressMessage = null;
                });

            delete $scope.contentUpdateTimer;
        },
        500);
    };

    $scope.$on("$destroy", function( event ) {
        cancelContentTimer();
    });

    $scope.showCommentEditor = function() {
        if (!$scope.editMode || !$scope.currentCalendarDay) return false;
        return true;
    };

    $scope.currentCalendarDayDate = function() {
        if (!$scope.currentCalendarDay) return "";

        return zeroPadded($scope.currentCalendarDay.number) + "." +
            zeroPadded(monthNumber+1) + "." +
            yearNumber;
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.doDiagnostics = function() {
        $scope.progressMessage = "Выполняю диагностику...";
        $http.get("/api/diagnostics")
            .success(function(data) {
                var alertType = 'success';
                if (data.status == 'ERROR') alertType = 'danger';
                $scope.alerts.push
                    ({type: alertType,
                    msg: data.details });
            })
            .error(function (error) {
                showError('Произошла ошибка. Не удалось провести диагностику.', error);
            })
            .finally(function() {
                $scope.progressMessage = null;
            });
    }

    function showError(prompt, error) {
        $scope.alerts.push
            ({type:'danger',
            msg: prompt + '\n' +
            'Технические данные: ' + extractErrorData(error)});
    };

    function extractErrorData(error) {
        return error.status + ' ' + error.error + ': ' + error.message;
    };

    function zeroPadded(number) {
        if (number >=10) return String(number);
        else return "0" + number;
    };

    function cancelContentTimer() {
        if ($scope.contentUpdateTimer) {
            $timeout.cancel($scope.contentUpdateTimer);
            delete $scope.contentUpdateTimer;
        }
    };
});


divoApp.service("dateService", function() {

    this.getDate = function() {
        return new Date();
    };

    this.calculateDayOffset = function(year, month) {
        // Date.getDay() returns Sunday = 0, Monday = 1, ... Saturday = 6
        // We have to translate it to 0..6 where 0 is Monday and 6 is Sunday.
        var firstDayOfMonth = new Date(year, month, 1);
        var dayOffsetAmerican = firstDayOfMonth.getDay();
        if (dayOffsetAmerican == 0) return 6;
        else return dayOffsetAmerican - 1;
    };

    this.daysInMonth = function(year, month) {
        return new Date(year, month+1, 0).getDate();
    };

    this.nextMonthAndYear = function(month, year) {
        var nextMonthDate = new Date(year, month+1, 1);
        return {
            month: nextMonthDate.getMonth(),
            year: nextMonthDate.getFullYear()
        };
    };

    this.previousMonthAndYear = function(month, year) {
        var prevMonthDate = new Date(year, month-1, 1);
        return {
            month: prevMonthDate.getMonth(),
            year: prevMonthDate.getFullYear()
        };
    };

});
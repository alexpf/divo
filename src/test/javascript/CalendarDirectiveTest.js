describe("Directive Test Suite", function () {

    var mockScope = {};
    var mockMonth = { year: 2015, number: 2,
        days: [ {id:1, number:1, eventType: 'REST'},
                {id:2, number:2, eventType: 'WORK', comment: 'Work'},
                {id:3, number:3, eventType: 'CONTEST', comment: 'Contest'}
              ]
        };
    var element = '<calendar month="currentMonth" show-current="editMode" ' +
        'day-offset="dayOffset" on-click="dayClick(id)"></calendar>';
    var compile;

    beforeEach(module("divoApp"));
    beforeEach(module("calendar"));

    beforeEach(inject(function ($compile, $rootScope) {

        mockScope = $rootScope.$new();
        mockScope.dayClick = function(index) {};
        mockScope.dayOffset = 1;
        mockScope.currentMonth = mockMonth;

        compile = $compile;
    }));

    it("shall select the classes for the calendar row cells properly when edit mode is off", function () {

        mockScope.editMode = false;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        expect(isolated.calendarRowCellClass(1)).toEqual("col-xs-1 col-xs-offset-2 text-center");
        expect(isolated.calendarRowCellClass(2)).toEqual("col-xs-1 text-center");

        isolated.currentCalendarDay = isolated.month.days[0];
        // Since the day indexes start with 1, and day offset is 1, the cell index 2 will be impacted
        expect(isolated.calendarRowCellClass(2)).toEqual("col-xs-1 text-center current-cell");
    });

    it("shall select the classes for the calendar day buttons properly", function () {

        mockScope.editMode = false;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        expect(isolated.calendarButtonClass(1)).toEqual("btn steel");
        expect(isolated.calendarButtonClass(2)).toEqual("btn steel text-center");
        expect(isolated.calendarButtonClass(3)).toEqual("btn yellow text-center");
        expect(isolated.calendarButtonClass(4)).toEqual("btn red text-center");
        expect(isolated.calendarButtonClass(5)).toEqual("btn steel");
    });

    it("shall select the calendar days to show/hide correctly", function () {

        mockScope.editMode = false;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        expect(isolated.showCalendarDayCell(1)).toEqual(false); // because dayOffset is 1
        expect(isolated.showCalendarDayCell(2)).toEqual(true);
        expect(isolated.showCalendarDayCell(3)).toEqual(true);
        expect(isolated.showCalendarDayCell(4)).toEqual(true);
        expect(isolated.showCalendarDayCell(5)).toEqual(false);
    });

    it("shall select the tooltip for a calendar day correctly", function () {

        mockScope.editMode = false;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        expect(isolated.getTooltip(1)).toEqual("");
        expect(isolated.getTooltip(2)).toEqual("");
        expect(isolated.getTooltip(3)).toEqual('Work');
    });

    it("shall return empty string in case calendar is not yet loaded", function () {

        mockScope.editMode = false;
        delete mockScope.currentMonth;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        expect(isolated.dayLabel(1)).toEqual('');
        expect(isolated.dayLabel(2)).toEqual('');
        expect(isolated.dayLabel(3)).toEqual('');
        expect(isolated.dayLabel(4)).toEqual('');
        expect(isolated.dayLabel(5)).toEqual('');
    });

    it("shall select the label for a calendar day correctly", function () {

        mockScope.editMode = false;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        expect(isolated.dayLabel(1)).toEqual('');
        expect(isolated.dayLabel(2)).toEqual('1');
        expect(isolated.dayLabel(3)).toEqual('2');
        expect(isolated.dayLabel(4)).toEqual('3');
        expect(isolated.dayLabel(5)).toEqual('');
    });

    it("shall do nothing on attempt " +
        "to click any (even non-existing) day in non-edit mode", function () {

        mockScope.editMode = false;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        spyOn(isolated, 'onClick');

        isolated.dayClick(0);
        expect(isolated.currentCalendarDay).toBeUndefined();
        expect(isolated.onClick).not.toHaveBeenCalled();

        isolated.dayClick(1);
        expect(isolated.currentCalendarDay).toBeUndefined();
        expect(isolated.onClick).not.toHaveBeenCalled();

        isolated.dayClick(5);
        expect(isolated.currentCalendarDay).toBeUndefined();
        expect(isolated.onClick).not.toHaveBeenCalled();
    });

    it("shall do nothing on attempt to click non-existing day in edit mode", function () {

        mockScope.editMode = true;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        spyOn(isolated, 'onClick');

        isolated.dayClick(0);
        expect(isolated.currentCalendarDay).toBeUndefined();
        expect(isolated.onClick).not.toHaveBeenCalled();

        isolated.dayClick(5);
        expect(isolated.currentCalendarDay).toBeUndefined();
        expect(isolated.onClick).not.toHaveBeenCalled();
    });

    it("shall change the current day and call click handler " +
        "when clicking on existing day in edit mode", function () {

        mockScope.editMode = true;
        element = compile(element)(mockScope);
        mockScope.$digest();

        var isolated = element.isolateScope();
        spyOn(isolated, 'onClick');

        isolated.dayClick(2);
        expect(isolated.currentCalendarDay).toEqual(isolated.month.days[0]);
        expect(isolated.onClick).toHaveBeenCalledWith({id:1});

        isolated.dayClick(3);
        expect(isolated.currentCalendarDay).toEqual(isolated.month.days[1]);
        expect(isolated.onClick).toHaveBeenCalledWith({id:2});
    });

});
describe("Controller Test Suite", function () {

    var mockScope = {};
    var controller;
    var httpBackend;
    var timeout;

    var mockGuest = {name: 'guest', roles: ['ROLE_GUEST']};
    var mockUser = {name: 'yana', roles: ['ROLE_USER']};
    var mockAdmin = {name: 'alexey', roles: ['ROLE_ADMIN','ROLE_USER']};
    var mockTags = [{ code: "YANA", russianTitle: "Yana" },
        { code: "OTHER", russianTitle: "Other" }];
    var mockMonth = { year: 2015, number: 2,
        days: [ {id:1, number:1, eventType: 'REST', comment: 'Rest'},
                {id:2, number:2, eventType: 'WORK', comment: 'Work'},
                {id:3, number:3, eventType: 'CONTEST', comment: 'Contest'}]
        };

    beforeEach(module("divoApp"));

    beforeEach(inject(function ($controller, $rootScope, $http, $httpBackend, $timeout, dateService) {
        mockScope = $rootScope.$new();
        httpBackend = $httpBackend;
        timeout = $timeout;

        dateService.getDate = function() { return new Date(2015, 1, 1); } // Feb 01, 2015

        controller = $controller("calendarCtrl", {
                        $scope: mockScope,
                        $http: $http,
                        $timeout: $timeout
                      });
    }));

    it("shall set up its (non-network-loaded) variables successfully", function () {
        expect(mockScope.editMode).toEqual(false);
        expect(mockScope.alerts).toEqual([]);
        expect(mockScope.data).toEqual({});
    });

    it("shall not allow to show calendar when month is not loaded", function () {
        expect(mockScope.showCalendar()).toEqual(false);
    });

    it("shall show the progress bar when the request is in progress", function () {
        expect(mockScope.progressShow()).toEqual(true);
    });

    it("shall load the user information successfully", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.userDetails).toEqual(mockUser);
        expect(mockScope.alerts).toEqual([]);
    });

    it("shall not show the progress when everything's loaded", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.progressShow()).toEqual(false);
    });

    it("shall recognize the admin successfully", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockAdmin);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.isAdmin()).toEqual(true);
    });

    it("shall not consider non-admin as admin", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.isAdmin()).toEqual(false);
    });

    it("shall return false to isAdmin() in case no user data available", function () {
        expect(mockScope.isAdmin()).toEqual(false);
    });

    it("shall not allow to show edit button in case no user data available", function () {
        expect(mockScope.showEditButton()).toEqual(false);
    });

    it("shall allow to show edit button to user", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.showEditButton()).toEqual(true);
    });

    it("shall not allow to show edit button to guest", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockGuest);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.showEditButton()).toEqual(false);
    });

    it("shall show edit button caption as 'Редактирование' initially", function () {
        expect(mockScope.editMode).toEqual(false);
        expect(mockScope.editButtonCaption()).toEqual('Редактирование');
    });

    it("shall not allow to show comment editor initially", function () {
        expect(mockScope.showCommentEditor()).toEqual(false);
    });

    it("shall toggle edit mode to 'ON' correctly", function () {
        mockScope.currentCalendarDay = mockMonth.days[0];
        mockScope.toggleEditMode();

        expect(mockScope.editMode).toEqual(true);
        expect(mockScope.editButtonCaption()).toEqual('Просмотр');
        expect(mockScope.currentCalendarDay).toEqual(undefined);
    });

    it("shall allow to show comment editor in edit mode when day is selected", function () {
        mockScope.toggleEditMode();
        mockScope.currentCalendarDay = mockMonth.days[0];
        expect(mockScope.showCommentEditor()).toEqual(true);
    });

    it("shall toggle edit mode to 'OFF' correctly", function () {
        mockScope.currentCalendarDay = mockMonth.days[0];
        mockScope.editMode = true;
        mockScope.toggleEditMode();

        expect(mockScope.editMode).toEqual(false);
        expect(mockScope.editButtonCaption()).toEqual('Редактирование');
        expect(mockScope.currentCalendarDay).toEqual(undefined);
    });

    it("shall toggle edit mode to 'ON' correctly", function () {
        mockScope.currentCalendarDay = mockMonth.days[0];
        mockScope.toggleEditMode();

        expect(mockScope.editMode).toEqual(true);
        expect(mockScope.editButtonCaption()).toEqual('Просмотр');
        expect(mockScope.currentCalendarDay).toEqual(undefined);
    });

    it("shall show the alert in case of user downloading error", function () {
        httpBackend.when("GET", "/api/user").respond(500, '');

        httpBackend.flush();
        expect(mockScope.userDetails).toEqual(undefined);
        expect(mockScope.alerts[0].msg)
            .toMatch(/Произошла ошибка\. Не удалось загрузить информацию о пользователе\..*/);
    });

    it("shall close the alert successfully", function () {
        httpBackend.when("GET", "/api/user").respond(500, '');
        httpBackend.flush();
        mockScope.closeAlert(0);

        expect(mockScope.alerts.length).toEqual(0);
    });

    it("shall load the list of tags successfully", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.tags[0]).toEqual(mockTags[1]);
        expect(mockScope.tags[1]).toEqual(mockTags[0]);
        expect(mockScope.alerts).toEqual([]);
    });

    it("shall return 'selected' class for the tag code YANA, after loading the taglist", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.tagItemClass('YANA')).toEqual('tag-item-selected');
        expect(mockScope.tagItemClass('OTHER')).toEqual('tag-item');
    });

    it("shall do nothing in case if the current tag is reselected", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        mockScope.selectCurrentTag('YANA');
        expect(mockScope.tagItemClass('YANA')).toEqual('tag-item-selected');
    });

    it("shall reload month if another tag is selected", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        mockScope.selectCurrentTag('OTHER');
        httpBackend.expectPOST(/\/api\/month.*tag=OTHER/);
        httpBackend.flush();
        expect(mockScope.tagItemClass('OTHER')).toEqual('tag-item-selected');
    });

    it("shall show the alert in case of tags downloading error", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(500, '');

        httpBackend.flush();
        expect(mockScope.tags).toEqual({});
        expect(mockScope.alerts[0].msg)
            .toMatch(/Произошла ошибка\. Не удалось загрузить список тегов\..*/);
    });


    it("shall load the current month successfully", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.data.currentMonth).toEqual(mockMonth);
        expect(mockScope.alerts).toEqual([]);
    });

    it("shall allow to show calendar when current month is loaded", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);

        httpBackend.flush();
        expect(mockScope.showCalendar()).toEqual(true);
    });

    it("shall show the alert in case of month downloading error", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(500, '');

        httpBackend.flush();
        expect(mockScope.data.currentMonth).toEqual(undefined);
        expect(mockScope.alerts[0].msg)
            .toMatch(/Произошла ошибка\. Не удалось загрузить календарь месяца\..*/);
    })

    it("shall return empty string for current calendar day date at the start", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        expect(mockScope.currentCalendarDayDate()).toEqual('');
    });

    it("shall format current calendar day date correctly", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        mockScope.currentCalendarDay = mockMonth.days[1];
        expect(mockScope.currentCalendarDayDate()).toEqual('02.02.2015');
        mockScope.currentCalendarDay = mockMonth.days[2];
        expect(mockScope.currentCalendarDayDate()).toEqual('03.02.2015');
    });

    it("shall send the day update request to backend after timeout, if comment is changed", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        mockScope.currentCalendarDay = mockMonth.days[0];
        mockScope.onCommentChange();
        expect(mockScope.contentUpdateTimer).toBeDefined();

        httpBackend.whenPUT('/api/day/1').respond(200);
        timeout.flush();
        expect(mockScope.progressMessage).toEqual('Обновляю описание события...');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
        expect(mockScope.contentUpdateTimer).toBeUndefined();
    });

    it("shall show error message, if comment update is failed", function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        mockScope.currentCalendarDay = mockMonth.days[0];
        mockScope.onCommentChange();

        httpBackend.whenPUT('/api/day/1').respond(500, '');
        timeout.flush();
        expect(mockScope.progressMessage).toEqual('Обновляю описание события...');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
        expect(mockScope.contentUpdateTimer).toBeUndefined();
        expect(mockScope.alerts[0].msg).toMatch(/Произошла ошибка\. Не удалось обновить описание события\..*/);
    });

    it('shall do nothing on click, in case if edit mode is off', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        var previousStatus = mockScope.data.currentMonth.days[0].eventType;
        mockScope.dayClick(1);
        expect(mockScope.currentCalendarDay).toBeUndefined();
        expect(mockScope.data.currentMonth.days[0].eventType).toEqual(previousStatus);
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall do nothing on click, in case if no month data is present', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(500, '');
        httpBackend.flush();
        mockScope.editMode = true;

        mockScope.dayClick(1);
        expect(mockScope.currentCalendarDay).toBeUndefined();
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall do nothing on click, in case if day index is too small', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;

        mockScope.dayClick(0);
        expect(mockScope.currentCalendarDay).toBeUndefined();
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall do nothing on click, in case if day index is too big', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;

        mockScope.dayClick(40);
        expect(mockScope.currentCalendarDay).toBeUndefined();
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall set the clicked day as current, if edit mode is ON and current day is not yet defined', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;
        mockScope.dayOffset = 0; // to simplify and avoid day offset calculations

        var previousStatus = mockScope.data.currentMonth.days[0].eventType;
        mockScope.dayClick(1);
        expect(mockScope.currentCalendarDay).toEqual(mockMonth.days[0]);
        expect(mockScope.data.currentMonth.days[0].eventType).toEqual(previousStatus);
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall set another day as current if the current day is already set but another day is clicked', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;
        mockScope.dayOffset = 0; // to simplify and avoid day offset calculations

        mockScope.currentCalendarDay = mockScope.data.currentMonth.days[0];
        mockScope.dayClick(2);
        expect(mockScope.currentCalendarDay).toEqual(mockScope.data.currentMonth.days[1]);
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall change the day status from REST to WORK', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;
        mockScope.dayOffset = 0; // to simplify and avoid day offset calculations

        mockScope.currentCalendarDay = mockScope.data.currentMonth.days[0];
        mockScope.dayClick(1);
        expect(mockScope.data.currentMonth.days[0].eventType).toEqual('WORK');
        expect(mockScope.progressMessage).toEqual('Обновляю статус дня...');
        httpBackend.whenPUT(/\/api\/day\/1/).respond(200,'');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall change the day status from WORK to CONTEST', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;
        mockScope.dayOffset = 0; // to simplify and avoid day offset calculations

        mockScope.currentCalendarDay = mockScope.data.currentMonth.days[1];
        mockScope.dayClick(2);
        expect(mockScope.data.currentMonth.days[1].eventType).toEqual('CONTEST');
        expect(mockScope.progressMessage).toEqual('Обновляю статус дня...');
        httpBackend.whenPUT(/\/api\/day\/2/).respond(200,'');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall change the day status from CONTEST back to REST', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;
        mockScope.dayOffset = 0; // to simplify and avoid day offset calculations

        mockScope.currentCalendarDay = mockScope.data.currentMonth.days[2];
        mockScope.dayClick(3);
        expect(mockScope.data.currentMonth.days[2].eventType).toEqual('REST');
        expect(mockScope.progressMessage).toEqual('Обновляю статус дня...');
        httpBackend.whenPUT(/\/api\/day\/3/).respond(200,'');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
    });

    it('shall show error message in case of network error', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();
        mockScope.editMode = true;
        mockScope.dayOffset = 0; // to simplify and avoid day offset calculations

        mockScope.currentCalendarDay = mockScope.data.currentMonth.days[0];
        mockScope.dayClick(1);
        expect(mockScope.data.currentMonth.days[0].eventType).toEqual('WORK');
        expect(mockScope.progressMessage).toEqual('Обновляю статус дня...');
        httpBackend.whenPUT(/\/api\/day\/1/).respond(500,'');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
        expect(mockScope.alerts[0].msg).toMatch(/Произошла ошибка. Не удалось обновить день..*/);
    });

    it('shall request the previous month after clicking Back button', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        httpBackend.expectPOST(/\/api\/month\?year=2015&number=1.*/).respond(200, mockMonth);
        mockScope.clickBack();
        httpBackend.flush();
    });

    it('shall request the next month after clicking Forward button', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        httpBackend.expectPOST(/\/api\/month\?year=2015&number=3.*/).respond(200, mockMonth);
        mockScope.clickForward();
        httpBackend.flush();
    });

    it('shall request the diagnostics after clicking Diagnostics button, ' +
        'and show success alert if diagnostics is successful', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        var details = 'Ok';
        httpBackend.expectGET('/api/diagnostics').respond(200, {status: 'SUCCESS', details: details});
        mockScope.doDiagnostics();
        expect(mockScope.progressMessage).toEqual('Выполняю диагностику...');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
        expect(mockScope.alerts[0].type).toEqual('success');
        expect(mockScope.alerts[0].msg).toEqual(details);
    });

    it('shall request the diagnostics after clicking Diagnostics button, ' +
        'and show error alert if diagnostics ends up with error', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        var details = 'Error';
        httpBackend.expectGET('/api/diagnostics').respond(200, {status: 'ERROR', details: details});
        mockScope.doDiagnostics();
        expect(mockScope.progressMessage).toEqual('Выполняю диагностику...');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
        expect(mockScope.alerts[0].type).toEqual('danger');
        expect(mockScope.alerts[0].msg).toEqual(details);
    });

    it('shall request the diagnostics after clicking Diagnostics button, ' +
        'and show error alert if diagnostics web request fails', function () {
        httpBackend.when("GET", "/api/user").respond(200, mockUser);
        httpBackend.when("GET", "/api/tag").respond(200, mockTags);
        httpBackend.whenPOST(/\/api\/month.*/).respond(200, mockMonth);
        httpBackend.flush();

        httpBackend.expectGET('/api/diagnostics').respond(500, '');
        mockScope.doDiagnostics();
        expect(mockScope.progressMessage).toEqual('Выполняю диагностику...');
        httpBackend.flush();
        expect(mockScope.progressMessage).toBeNull();
        expect(mockScope.alerts[0].type).toEqual('danger');
        expect(mockScope.alerts[0].msg).toMatch(/Произошла ошибка. Не удалось провести диагностику..*/);
    });

});